package dev.felipheallef.newsapp.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import dev.felipheallef.newsapp.util.getOrAwaitValue
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NewsListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun test_articleListLiveData() {
        // Create the ViewModel
        val viewModel = NewsListViewModel(ApplicationProvider.getApplicationContext())

        // Get the LiveData
        val value = viewModel.articleList.getOrAwaitValue()

        assertEquals(10, value.size)
        assertEquals("bbc-news", value.first().source.id)
        assertEquals("BBC News", value.first().source.name)

    }

}