package dev.felipheallef.newsapp

import com.google.gson.Gson
import dev.felipheallef.newsapp.data.Article
import dev.felipheallef.newsapp.data.Source
import org.junit.Test

import org.junit.Assert.*
import java.time.Instant
import java.util.*

/**
 * Unit tests for JSON parsing.
 *
 * @author Feliphe Allef
 */
class GsonUnitTest {

    @Test
    fun testSourceObject() {
        val json = "{\"id\":\"bbc-news\",\"name\":\"BBC News\"}"
        val source = Gson().fromJson(json, Source::class.java)

        assertEquals("bbc-news", source.id)
        assertEquals("BBC News", source.name)
    }

    @Test
    fun testArticleObject() {
        val json = "{\"source\":{\"id\":\"bbc-news\",\"name\":\"BBC News\"},\"author\":\"BBC News\",\"title\":\"Joint Europe-Russia Mars rover project is parked\",\"description\":\"European Space Agency delegates suspend the project to send a UK-built robot to Mars this year.\",\"url\":\"http://www.bbc.co.uk/news/science-environment-60782932\",\"urlToImage\":\"https://ichef.bbci.co.uk/news/1024/branded_news/9C05/production/_108514993__ab19902.jpg\",\"publishedAt\":\"2022-03-17T16:22:20.0069937Z\",\"content\":\"Jonathan AmosScience correspondent@BBCAmoson Twitter\\r\\nImage source, Max Alexander\\r\\nImage caption, The rover project was first approved in 2005\\r\\nThe decision has been made. Europe's Mars rover will no… [+3179 chars]\"}"
        val article = Gson().fromJson(json, Article::class.java)

        assertEquals("BBC News", article.author)
        assertEquals("Joint Europe-Russia Mars rover project is parked", article.title)
        assertEquals(Date(1647534140006), article.publishedAt)
    }
}