package dev.felipheallef.newsapp.api.service

import dev.felipheallef.newsapp.data.NewsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {

    @GET("top-headlines")
    fun getTopHeadlines(
        @Query("sources") sources: String,
        @Query("sortBy") sortBy: String = "publishedAt"
    ): Call<NewsResponse>
}