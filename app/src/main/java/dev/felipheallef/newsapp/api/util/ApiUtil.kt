package dev.felipheallef.newsapp.api.util

import dev.felipheallef.newsapp.api.interceptor.ApiInterceptor
import dev.felipheallef.newsapp.api.service.NewsService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiUtil {

    val httpClient = OkHttpClient.Builder()
        .addInterceptor(ApiInterceptor())

    val retrofit: Retrofit = Retrofit.Builder()
        .client(httpClient.build())
        .baseUrl("https://newsapi.org/v2/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val service: NewsService = retrofit.create(NewsService::class.java)

}