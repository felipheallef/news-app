package dev.felipheallef.newsapp.data

/**
 * News API Response Entity
 * @param status The request status (e.g. ok)
 * @param totalResults The total number of results
 * @param articles A list of returned articles
 */
data class NewsResponse(
    val status: String,
    val totalResults: Int,
    val articles: List<Article>,
)