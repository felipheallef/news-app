package dev.felipheallef.newsapp.data

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import dev.felipheallef.newsapp.R
import dev.felipheallef.newsapp.databinding.ListItemArticleCardBinding
import java.io.Serializable
import java.util.*

class Article(
    val author: String?,
    val title: String,
    val content: String,
    val description: String,
    val url: String,
    val urlToImage: String?,
    val publishedAt: Date,
    val source: Source,
) : AbstractBindingItem<ListItemArticleCardBinding>(), Serializable {

    override val type: Int
        get() = publishedAt.time.toInt()

    override fun bindView(binding: ListItemArticleCardBinding, payloads: List<Any>) {
        binding.txtArticleTitle.text = title

        urlToImage?.also {
            Glide.with(binding.imgArticleHeading.context)
                .load(it).into(binding.imgArticleHeading)
        }
    }

    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup?
    ): ListItemArticleCardBinding {
        return ListItemArticleCardBinding.inflate(inflater, parent, false)
    }
}