package dev.felipheallef.newsapp.data

import java.io.Serializable

/**
 * News provider/source
 *
 * @param id The provider id
 * @param name The provider display name
 */
data class Source(
    val id: String?,
    val name: String,
) : Serializable