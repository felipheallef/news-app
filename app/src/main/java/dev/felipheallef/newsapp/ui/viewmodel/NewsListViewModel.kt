package dev.felipheallef.newsapp.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import dev.felipheallef.newsapp.BuildConfig
import dev.felipheallef.newsapp.R
import dev.felipheallef.newsapp.api.util.ApiUtil
import dev.felipheallef.newsapp.data.Article
import dev.felipheallef.newsapp.data.NewsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsListViewModel(application: Application) : AndroidViewModel(application) {

    val articleList = MutableLiveData<List<Article>>()
    val message = MutableLiveData<String?>()

    init {
        getTopHeadlines()
    }

    private fun getTopHeadlines() {
        val request = ApiUtil.service.getTopHeadlines(BuildConfig.SOURCE)
        message.value = getApplication<Application>().resources.getString(R.string.label_loading)

        request.enqueue(object : Callback<NewsResponse> {
            override fun onResponse(call: Call<NewsResponse>, response: Response<NewsResponse>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        if (it.status == "ok") {
                            articleList.postValue(it.articles)
                            message.value = null
                        }
                    }
                } else {
                    message.value = response.errorBody()?.string()
                }

            }

            override fun onFailure(call: Call<NewsResponse>, t: Throwable) {
                message.value = t.message
            }
        })
    }
}