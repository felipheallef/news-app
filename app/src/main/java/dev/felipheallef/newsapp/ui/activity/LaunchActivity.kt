package dev.felipheallef.newsapp.ui.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.AuthenticationResult
import androidx.core.content.ContextCompat
import dev.felipheallef.newsapp.R
import java.util.concurrent.Executor

class LaunchActivity : AppCompatActivity() {

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val biometricManager = BiometricManager.from(this)

            when (biometricManager.canAuthenticate(BIOMETRIC_STRONG)) {
                BiometricManager.BIOMETRIC_SUCCESS -> {
                    Log.d("MY_APP_TAG", "App can authenticate using biometrics.")
                    askUserFingerprint()
                }
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                    Log.e("MY_APP_TAG", "No biometric features available on this device.")
                    launchApplication()
                }
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                    Log.e(TAG, "Biometric features are currently unavailable.")
                    launchApplication()
                }
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    Log.e(TAG, "No fingerprint available, skipping.")
                    launchApplication()
                }
            }
        } else {
            if (packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT))
                askUserFingerprint()
            else launchApplication()
        }
    }

    /**
     * Redirects the user to main screen
     */
    fun launchApplication() {
        Intent(this, MainActivity::class.java).also {
            startActivity(it)
            finish()
        }
    }

    /**
     * Shows fingerprint scanner prompt
     */
    private fun askUserFingerprint() {
        executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.i(TAG, "Authentication error: $errString")

                    // Check if fingerprint is configured on devices with Android 10 or below
                    if (errorCode == BiometricPrompt.ERROR_NO_BIOMETRICS ||
                        errorCode == BiometricPrompt.ERROR_HW_NOT_PRESENT
                    ) {
                        launchApplication()
                    } else {
                        finish()
                    }
                }

                override fun onAuthenticationSucceeded(result: AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    launchApplication()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Log.i(TAG, "Authentication failed")
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.label_authentication))
            .setSubtitle(getString(R.string.label_authentication_desc))
            .setNegativeButtonText(getString(R.string.action_cancel))
            .setAllowedAuthenticators(BIOMETRIC_STRONG)
            .build()

        biometricPrompt.authenticate(promptInfo)
    }

    companion object {
        const val TAG = "LaunchActivity"
    }
}