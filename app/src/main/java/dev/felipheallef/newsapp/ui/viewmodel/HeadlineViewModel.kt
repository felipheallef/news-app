package dev.felipheallef.newsapp.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import dev.felipheallef.newsapp.data.Article
import java.text.DateFormat

class HeadlineViewModel() : ViewModel() {

    val article = MutableLiveData<Article>()

    val publishedAt: LiveData<String> = liveData {
        article.value?.publishedAt?.also {
            val date = DateFormat.getDateInstance().format(it)
            emit(date)
        }
    }

    fun init(value: Article) {
        article.value = value
    }
}