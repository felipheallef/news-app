package dev.felipheallef.newsapp.ui.activity

import android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import dev.felipheallef.newsapp.data.Article
import dev.felipheallef.newsapp.databinding.ActivityHeadlineBinding
import dev.felipheallef.newsapp.ui.viewmodel.HeadlineViewModel

class HeadlineActivity : AppCompatActivity() {

    private val binding: ActivityHeadlineBinding by lazy {
        ActivityHeadlineBinding.inflate(layoutInflater)
    }

    private val viewModel: HeadlineViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setSupportActionBar(binding.topAppBar)

        val article = intent.getSerializableExtra("article") as Article
        Log.i(TAG, article.title)

        supportActionBar?.title = article.source.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        article.urlToImage?.also {
            Glide.with(this)
                .load(it).into(binding.imgArticleHeading)
        }

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            binding.txtArticleContent.justificationMode = JUSTIFICATION_MODE_INTER_WORD
        }

        viewModel.init(article)
    }

    companion object {
        const val TAG = "HeadlineActivity"
    }
}