package dev.felipheallef.newsapp.ui.activity

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.binding.BindingViewHolder
import com.mikepenz.fastadapter.listeners.ClickEventHook
import dev.felipheallef.newsapp.R
import dev.felipheallef.newsapp.data.Article
import dev.felipheallef.newsapp.databinding.ActivityMainBinding
import dev.felipheallef.newsapp.databinding.ListItemArticleCardBinding
import dev.felipheallef.newsapp.ui.decoration.GridSpacingItemDecoration
import dev.felipheallef.newsapp.ui.decoration.MarginItemDecoration
import dev.felipheallef.newsapp.ui.viewmodel.NewsListViewModel

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val viewModel: NewsListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setSupportActionBar(binding.topAppBar)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel.articleList.observe(this) {
            if (it.isNotEmpty())
                supportActionBar?.title = it.first().source.name

            setupList(it)
        }

    }

    private fun setupList(articles: List<Article>) {
        val itemAdapter = ItemAdapter<Article>()
        val fastAdapter = FastAdapter.with(itemAdapter)

        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.grid_layout_margin)

        // Set the layout to either linear or grid depending on the screen orientation
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            binding.recyclerView.layoutManager = LinearLayoutManager(this)
            binding.recyclerView.addItemDecoration(MarginItemDecoration(spacingInPixels))
        } else {
            binding.recyclerView.layoutManager = GridLayoutManager(this, 2)
            binding.recyclerView.addItemDecoration(
                GridSpacingItemDecoration(
                    2,
                    spacingInPixels,
                    true,
                    0
                )
            )
        }

        // Handles item click events
        fastAdapter.addEventHook(object : ClickEventHook<Article>() {
            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return if (viewHolder is BindingViewHolder<*> && viewHolder.binding is ListItemArticleCardBinding) {
                    (viewHolder.binding as ListItemArticleCardBinding).listItemArticle
                } else {
                    null
                }
            }

            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<Article>,
                item: Article
            ) {
                // TODO: handle article click
                Log.i("MainActivity", "clicked")
                val intent = Intent(this@MainActivity, HeadlineActivity::class.java).apply {
                    putExtra("article", item)
                }

                startActivity(intent)
            }
        })

        //set our adapters to the RecyclerView
        binding.recyclerView.adapter = fastAdapter

        itemAdapter.set(articles)
    }
}