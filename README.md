# News App

An app that shows top headlines from a specific source.

## Installing and testing

Click on **Downloads** on the sidebar, click on **app-bbcNews-release.apk** to download the main flavor (BBC News). There are also others such as Globo, IGN and The Verge.

## Building from source

1. First, clone the repository:
   ````
   git clone https://bitbucket.org/felipheallef/news-app.git
   ````

2. Edit local.properties to set your News API Key:
   ````
   sdk.dir=/path/to/your/sdk
   NEWS_API_KEY=YOUR_API_KEY
   ````

   Replace YOUR_API_KEY with your API KEY obtained from newsapi.org.